/**
 * View service to render a podcast details and episode list
 * @param {number} [podcast] The identifier of the podcast.
 */
class PodcastView extends View {
    constructor(podcast) {
        super(podcast);
        this._data = {};
    }

    /**
     * Transforms a remote service response to remove unnecessary properties.
     * @param {Array} data An array with the response data.
     * @private
     */
    _transform(data) {
        this._data.podcast = {
            id: data.collectionId,
            title: data.collectionName,
            artist: data.artistName,
            img: data.artworkUrl100
        }
    }

    /**
     * Formats a date to dd/mm/yyy
     * @param {string} dateStr A date string to format
     * @returns {string}
     * @private
     */
    static _formatDate(dateStr) {
        let date = new Date(dateStr);
        return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    }

    _getDuration(item) {
        let items = Array.prototype.slice.call(item.querySelectorAll('*')).filter(e => e.tagName === 'itunes:duration');
        if (items.length > 0) {
            return items[0].innerHTML;
        } else {
            return '--:--'
        }
    }

    /**
     * Parses the podcast RSS feed to get the episode list, and the podcast description
     * @param {Document} data An XML document describing the feed
     * @private
     */
    _parseFeed(data) {
        this._data.episodes = [];
        this._data.podcast.description = encodeURI(data.querySelector('description').innerHTML);
        data.querySelectorAll('item').forEach(item => {
            this._data.episodes.push({
                title: item.querySelector('title').innerHTML,
                description: encodeURI(item.querySelector('description').innerHTML.replace(/<!\[CDATA\[(.*)]]>/, '$1')),
                duration: this._getDuration(item),
                date: PodcastView._formatDate(item.querySelector('pubDate').innerHTML),
                link: item.querySelector('enclosure').getAttribute('url')
            });
        });
        Podcaster.storage.save(this._podcast, this._data);
    }

    /**
     * Gets the RSS feed for the podcast with cors protection.
     * @param {string} feed The feed URL
     * @returns {Promise}
     * @private
     */
    _getFeed(feed) {
        return $.ajax({url: `${Podcaster.corsService}/${feed}?format=xml`})
            .then(data => {
                return this._parseFeed(data);
            }).catch(error => window.console.log(error));
    }

    /**
     * Gets the data from an external service
     * @virtual
     */
    getData() {
        if (!Podcaster.storage.isObsolete(this._podcast)) {
            this._data = Podcaster.storage.load(this._podcast);
            return Promise.resolve(true);
        } else {
            return this.request(`https://itunes.apple.com/lookup?id=${this._podcast}`)
                .then(data => {
                    this._transform(data.results[0]);
                    return data.results[0].feedUrl;
                })
                .then(feed => {
                    return this._getFeed(feed);
                })
                .catch(error => window.console.log(error));
        }
    }

    /**
     * Validates that the data retrieved is suitable for rendering
     * @virtual
     */
    _validate() {
        return true;
    }

    /**
     * Adds listeners to handle the click event of every podcast card.
     * @private
     */
    _addListeners() {
        document.querySelectorAll('td a').forEach(e => {
            e.addEventListener('click', event => {
                event.preventDefault();
                Podcaster.router.navigate(this._podcast, event.currentTarget.dataset.index);
            });
        })
    }

    /**
     * Generates the HTML for the episode list.
     * @returns {string}
     * @private
     */
    _renderItems() {
        let episodes = this._data.episodes.reduce((prev, episode, index) => {
            return `${prev}\n<tr><td><a href="" data-index="${index}">${episode.title}</a></td><td>${episode.date}</td><td>${episode.duration}</td></tr>`;
        }, '');

        return  `<div class="card card-shadow m-3"><div class="card-body"><table class="table table-striped table-hover">
            <thead><tr><th >Title</th><th class="podcast-date">Date</th><th class="podcast-length">Duration</th></tr></thead>
            <tbody>${episodes}</tbody>
            </table></div></div>`;
    }

    /**
     * Renders the view within the container
     * @param {string} [container] CSS selector to specify the container
     */
    render(container = '.contents') {
        this.getData()
            .then(e => {
                this._parsedTemplate = `
            <div class="d-flex flex-row">
                ${this.renderPodcastDetails()}
                <div class="podcast-episodes-container d-flex flex-column col">
                    <div class="card card-shadow m-3">
                        <div class="card-body"><h3>Episodes: ${this._data.episodes.length}</h3>
                        </div>
                    </div>
                    ${this._renderItems()}                
               </div>
            </div>`;
                super.render();
                this._addListeners();
                this.toggleLoadingAnimation();
            });
    }
}