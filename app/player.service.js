class PodcastPlayer {
    constructor(url) {
        this._url = url;
        this._elements = {};
    }

    /**
     * Formats duration to hh:mm:ss
     * @param {number} duration Podcast duration in seconds
     * @returns {string}
     * @private
     */
    static _formatTime(duration) {
        let lpad = n => ('00' + n).slice(1);
        let total = parseInt(duration, 10);
        let h = Math.floor(total/3600);
        let m = Math.floor((total % 3600) / 60);
        let s = total - 3600*h - 60*m;
        return `${lpad(h)}:${lpad(m)}:${lpad(s)}` ;
    }

    /**
     * Caches the player controls
     */
    addElements() {
        this._elements = {
            buttons: {
                play: document.querySelector('.podcast-play'),
                pause: document.querySelector('.podcast-pause'),
                mute: document.querySelector('.podcast-mute')
            },
            meters: {
                elapsedTime: document.querySelector('.podcast-elapsed-time'),
                total: document.querySelector('.podcast--duration'),
                progress: document.querySelector('.podcast-progress')
            },
            audio: document.querySelector('.podcast-player')
        };

        this._elements.audio.setAttribute('crossorigin', 'anonymous');
        this._elements.audio.crossOrigin = 'anonymous';
        return this;
    }

    /**
     * Adds listeners for the audio player controls.
     * @private
     */
    addListeners() {
        this._elements.audio.addEventListener('loadedmetadata', event => {
            this._elements.meters.progress.setAttribute('max', Math.floor(this._elements.audio.duration).toString());
            this._elements.meters.total.textContent  = PodcastPlayer._formatTime(this._elements.audio.duration);
        });

        this._elements.audio.addEventListener('timeupdate', event =>  {
            this._elements.meters.progress.setAttribute('value', this._elements.audio.currentTime);
            this._elements.meters.elapsedTime.textContent  = PodcastPlayer._formatTime(this._elements.audio.currentTime);
        });

        this._elements.buttons.play.addEventListener('click', event => {
            this._elements.buttons.play.display = 'none';
            this._elements.buttons.pause.display = 'inline-block';
            this._elements.buttons.play.focus();
            this._elements.audio.play()
        });

        this._elements.buttons.play.addEventListener('click', event => {
            this._elements.buttons.pause.display = 'none';
            this._elements.buttons.play.display = 'inline-block';
            this._elements.buttons.pause.focus();
            this._elements.audio.pause()
        });

        this._elements.meters.progress.addEventListener('click', event => {
            this._elements.audio.currentTime = Math.floor(this._elements.audio.duration) * (event.offsetX / event.target.offsetWidth);
        }, false);

        this._elements.buttons.mute.addEventListener('click', event =>  {
            if(this._elements.audio.muted) {
                this._elements.audio.muted = false;
                this._elements.buttons.mute.querySelector('.fa').classList.remove('fa-volume-off');
                this._elements.buttons.mute.querySelector('.fa').classList.add('fa-volume-up');
            } else {
                this._elements.audio.muted = true;
                this._elements.buttons.mute.querySelector('.fa').classList.remove('fa-volume-up');
                this._elements.buttons.mute.querySelector('.fa').classList.add('fa-volume-off');
            }
        }, false);
    }

    /**
     * Generates the HTML for the audio player.
     * @returns {string}
     */
    render() {
        return `<div class="p-player">
                    <div class="p-player-controls">
                        <button class="podcast-play"><i class="fa fa-play"></i></button>
                        <button class="podcast-pause"><i class="fa fa-pause"></i></button>
                        <span class="podcast-elapsed-time p-time">00:00</span>
                        <progress class="podcast-progress" value="0"></progress>
                        <span class="podcast--duration p-time">00:00</span>
                        <button class="podcast-mute"><i class="fa fa-volume-up"></i></button>
                    </div>
                    <audio class="podcast-player" src="${Podcaster.corsService}/${this._url}"></audio>
                </div>`
    }
}
