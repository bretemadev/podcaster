class StorageService {
    constructor() {
        this._storage = window.localStorage;
    }

    /**
     * Sets the desired browser storage.
     * @param {string} storageLocation Can be 'local' or 'session'
     */
    set storage(storageLocation) {
        if (storageLocation === 'local') {
            this._storage = window.localStorage;
        } else {
            this._storage = window.sessionStorage;
        }
    }

    /**
     * Updates the timestamp of the last save for the given key
     * @param {string} key The stored key
     * @private
     */
    _update(key) {
        let lastSave = this._storage.getItem(StorageService._prefix('lastSave'));
        lastSave = lastSave ? JSON.parse(lastSave) : {};
        lastSave[key] = Date.now().toString();
        this._storage.setItem(StorageService._prefix('lastSave'), JSON.stringify(lastSave));
    }

    /**
     * Prefixes a key with the app name before saving or loading
     * @param {string} key The key to prefix
     * @returns {string}
     * @static
     */
    static _prefix(key) {
        return `PODCASTER.${key}`;
    }

    /**
     * Checks if the stored elements are more than a day old. If no parameter is supplied, the result is wether the
     * podcast list is obsolete.
     * @param {string} [key] A podcast identifier
     */
    isObsolete(key = 'list') {
        let lastSave = this._storage.getItem(StorageService._prefix('lastSave'));
        if (lastSave !== null) {
            lastSave = JSON.parse(lastSave);
            return (Date.now() - (lastSave[key] ||0)) > 86400000;
        }

        return true;
    }

    /**
     * Gets an stored element.
     * @param {string} key The storage key to retrieve.
     */
    load(key) {
        let value = this._storage.getItem(StorageService._prefix(key));
        return (value ? JSON.parse(value) : null);
    }

    /**
     * Stores an object in the browser storage, serializing it before saving
     * @param {string} key The key to store the value with.
     * @param {object} value The value to save.
     */
    save(key, value) {
        let serialized = JSON.stringify(value);
        this._storage.setItem(StorageService._prefix(key), serialized);
        this._update(key);
    }

    /**
     * Clears all items stored by the app
     */
    clear() {
        this._storage.clear();
    }
}