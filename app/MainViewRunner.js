/**
 * View service that renders and manages a podcast list.
 */
class MainView extends View {
    constructor() {
        super();
        this._data = [];
        this._elements = {};
    }

    /**
     * Transforms a remote service response to remove unnecessary properties.
     * @param {Array} data An array with the response data.
     * @private
     */
    _transform(data) {
        data.feed.entry.forEach((podcast, i) => {
            this._data.push({
                title: podcast['im:name'].label,
                author: podcast['im:artist'].label,
                img: podcast['im:image'][0].label,
                id: podcast.id.attributes['im:id']
            })
        });
    }

    /**
     * Gets the data from an external service
     * @virtual
     */
    getData() {
        if (!Podcaster.storage.isObsolete()) {
            this._data = Podcaster.storage.load('list');
            return Promise.resolve(true);
        } else {
            return this.request('https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json')
                .then(data => {
                    this._transform(data);
                    Podcaster.storage.save('list', this._data);
                    return true;
                })
                .catch(error => {return error;});
        }
    }

    /**
     * Validates that the data retrieved is suitable for rendering
     * @virtual
     */
    _validate() {
        return this._data !== null && this._data instanceof Array;
    }

    _addElements() {
        document.querySelectorAll('.podcast').forEach(element => {
            let id = element.dataset.podcastId;
            this._elements[id] = element;
        });
    }


    /**
     * Adds listeners to handle the click event of every podcast card.
     * @private
     */
    _addListeners() {
        this._data.forEach(p => {
            this._elements[p.id].addEventListener('click', event => {
                let item = event.currentTarget;
                Podcaster.router.navigate(item.dataset.podcastId);
            });
        });

        document.querySelector('#podcast-filter').addEventListener('keyup', (event) => {
            this._filterPodcasts(event.srcElement.value);
        });
    }

    _filterPodcasts(text) {
        let re = new RegExp(`.*${text}.*`, 'i'), count = 0;
        this._data
            .forEach(p => {
                let visible = re.test(p.author) || re.test(p.title);
                if (visible) {
                    this._elements[p.id].style.display = 'block';
                    count++;
                } else {
                    this._elements[p.id].style.display = 'none';
                }
            });
        document.querySelector('.badge.badge-primary.filter ').innerHTML = count;
    }

    /**
     * Generates the HTML of every element.
     * @returns {string}
     * @private
     */
    _renderItems() {
        return this._data.reduce((prev, podcast) => {
            return `
                ${prev}
                <div class="card card-shadow col-2 m-2 mt-5 mb-5 podcast" data-podcast-search="${podcast.title}${podcast.author}" data-podcast-id="${podcast.id}">
                    <img class="card-top-img rounded-circle" height="100" width="100"
                        src="${podcast.img}" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">${podcast.title}</h4>
                        <p class="card-text">${podcast.author}</p>
                    </div>
                </div>`;
        }, '');
    }

    /**
     * Renders the view within the container
     * @param {string} [container] CSS selector to specify the container
     */
    render(container = '.contents') {
        this.getData()
            .then(e => {
                this._parsedTemplate = `
                    <div class="d-flex flex-row justify-content-end m-3">
                        <div class="p-2"><span class=" badge badge-primary filter p-2">100</span></div>
                        <div class="p-2 col-2 filter"><input class="form-control" name="podcast-filter" id="podcast-filter" type="text" value="" placeholder="Filter"></div>
                    </div>
                    <div class=" m-5 d-flex justify-content-between align-items-stretch flex-wrap ">${this._renderItems()}</div>`;
                super.render();
                this._addElements();
                this._addListeners();
                this.toggleLoadingAnimation();
            })
            .catch(error => {
                window.console.log(error);
            });
    }
}