/**
 * View service to render and play a podcast episode
 * @param {number} [podcast] The identifier of the podcast.
 * @param {number} [episode] The identifier of the podcast episode.
 */
class EpisodeView extends View {
    constructor(podcast, episode) {
        super(podcast, episode);
        this._player = null;
    }

    /**
     * Transforms a remote service response to remove unnecessary properties.
     * @param {Array} data An array with the response data.
     * @private
     */
    _transform(data) {
    }

    /**
     * Gets the data from an external service
     * @virtual
     */
    getData() {
        let data = Podcaster.storage.load(this._podcast);
        this._data = {
            podcast: data.podcast,
            episode: data.episodes[this._episode]
        };
    }

    /**
     * Validates that the data retrieved is suitable for rendering
     * @virtual
     */
    _validate() {
        return true;
    }

    /**
     * Adds listeners to handle the click event of every podcast card.
     * @private
     */
    _addListeners() {
        document.querySelectorAll('.podcast-hover').forEach(element => {
            element.addEventListener('click', event => {
                Podcaster.router.navigate(this._podcast);
            });
        });
    }

    /**
     * Generates the HTML of the episode player.
     * @returns {string}
     * @private
     */
    _renderPodcast() {
        this._player = new PodcastPlayer(this._data.episode.link);
        return `<div class="podcast-description-container">
                    <div class="card card-shadow m-3">
                        <div class="p-2 card-title">
                            <h4>${this._data.episode.title}</h4>
                        </div>
                        <div class="card-body">
                            ${decodeURI(this._data.episode.description)}
                        </div>
                        <div class="card-body">
                            ${this._player.render()}
                        </div>
                    </div>
                </div>`;
    }

    /**
     * Renders the view within the container
     * @param {string} [container] CSS selector to specify the container
     */
    render(container = '.podcast-episodes-container') {
        this.getData();
        this._parsedTemplate = `
            <div class="d-flex flex-row">
                ${this.renderPodcastDetails(true)}
                <div class="podcast-episodes-container d-flex flex-column col">
                    ${this._renderPodcast()}                
               </div>
            </div>`;
        super.render();
        this._addListeners();
        this._player.addElements().addListeners();
        this.toggleLoadingAnimation();
    }
}