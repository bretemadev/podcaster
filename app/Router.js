/**
 * Router service to handle view changes.
 */
class Router {
    constructor() {
        this._routes = {
            main: () => {
                return {
                    location: '/',
                    runner: new MainView()
                };
            },
            podcast: podcast => {
                return {
                    location: `/podcast/${podcast}`,
                    runner: new PodcastView(podcast)
                };
            },
            episode: (podcast, episode) => {
                return {
                    location: `/podcast/${podcast}/episode/${episode}`,
                    runner: new EpisodeView(podcast, episode)
                };
            }
        };
        this._addListeners();
    }

    /**
     * Gets the appropriate route from the available routes.
     * @param {number} [podcast] The identifier of the podcast.
     * @param {number} [episode] The identifier of the podcast episode.
     * @returns {Object} The selected route.
     * @private
     */
    _getRoute(podcast = -1, episode = -1) {
        if (parseInt(podcast) > -1) {
            if (parseInt(episode) > -1) {
                return this._routes.episode(podcast, episode);
            } else {
                return this._routes.podcast(podcast);
            }
        } else {
            return this._routes.main();
        }
    }

    /**
     * popstate handler
     */
    _popstate(event) {
        if (window.history.state !== null) {
            this.navigate(event.state.podcast, event.state.episode);
        } else {
            window.history.back();
        }
    }

    /**
     * Adds event listeners to the popstate history event and to the app name link
     * @private
     */
    _addListeners() {
        window.onpopstate = this._popstate.bind(this);
        document.querySelector('div.podcaster a').addEventListener('click', event => {
            event.preventDefault();
            this.navigate();
        });
    }

    /**
     * Navigates to the desired route.
     * @param {number} [podcast] The identifier of the podcast.
     * @param {number} [episode] The identifier of the podcast episode.
     */
    navigate(podcast = -1, episode = -1) {
        let route = this._getRoute(podcast, episode),
            historyState = {
                podcast,
                episode
            };

        if (route.location !== '/') {
            window.history.pushState(historyState, "Podcaster", route.location);
        } else {
            window.history.replaceState(historyState, "Podcaster", route.location);
        }
        route.runner.run();
    }
}