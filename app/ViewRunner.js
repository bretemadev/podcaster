/**
 * Generic view service with many virtual methods to be overridden by child classes to handle specific aspects of each view.
 * @param {number} [podcast] The identifier of the podcast.
 * @param {number} [episode] The identifier of the podcast episode.
 */
class View {
    constructor(podcast = -1, episode = -1) {
        this._podcast = podcast;
        this._episode = episode;
        this._parsedTemplate = '';
        this._loadingAnimation = document.querySelector('.podcaster-loading');
    }

    /**
     * Validates that the data retrieved is suitable for rendering
     * @virtual
     */
    _validate() {
    }

    /**
     * Gets the data from an external service
     * @virtual
     */
    getData() {
    }

    /**
     * Generates the HTML for the podcast details
     * @param {boolean} [addLinks=false] Wether to add links to the podcast or not
     * @returns {string}
     */
    renderPodcastDetails(addLinks) {
        let linkClassName = addLinks ? 'podcast-hover' : '';
        return `<div class="podcast-description-container col-3">
                    <div class="card card-shadow m-3">
                        <div class="d-flex justify-content-center mt-5">                            
                            <img class="${linkClassName} p-2" height="200" width="200"  src="${this._data.podcast.img}" alt="Card image cap">
                        </div>
                        <div class="card-body p-3 mt-2">
                            <h4 class="${linkClassName}">${this._data.podcast.title}</h4>
                            <p class="font-italic">by ${this._data.podcast.artist}</p>
                        </div>
                        <div class="card-footer p-3">
                            <p class="font-weight-bold">Description:</p>
                            <div class="card-text">${decodeURI(this._data.podcast.description).replace(/<!\[CDATA\[(.*)]]>/, '$1')}</div>
                        </div>
                    </div>
                </div>`;
    }

    /**
     * Renders the view within the container
     * @param {string} [container] CSS selector to specify the container
     */
    render(container = '.contents') {
        document.querySelector(container).innerHTML = this._parsedTemplate;
    }

    /**
     * Performs a request to an external service
     * @param {string} endpoint The external service endpoint
     * @returns {Promise} A promise with the resolved data
     * @protected
     */
    request(endpoint) {
        return $.getJSON(endpoint, {})
            .then(data => {
                return data
            })
            .catch(error => {
                window.console.error(`An error has happened while trying to access the service at: ${endpoint}`);
                window.console.error(error);
            });
    }

    /**
     * Toggles the loading indicator
     */
    toggleLoadingAnimation() {
        if (this._loadingAnimation.style.display === 'block') {
            this._loadingAnimation.style.display = 'none';
        } else {
            this._loadingAnimation.style.display = 'block';
        }
    }

    /**
     * Executes the view runner
     */
    run() {
        this.toggleLoadingAnimation();
        this.render();
    }
}