window.Podcaster = (function() {
    return {
        storage: new StorageService(),
        router: new Router(),
        // corsService: 'http://crossorigin.me'
        corsService: 'https://cors-anywhere.herokuapp.com'
    }
})();

document.addEventListener('DOMContentLoaded', () => {
    Podcaster.router.navigate();
});
