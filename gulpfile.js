const
    gulp = require('gulp'),
    exec = require('child_process').exec,
    del = require('del'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify-es').default,
    files= [
        'app/storage.service.js',
        'app/player.service.js',
        'app/ViewRunner.js',
        'app/EpisodeViewRunner.js',
        'app/MainViewRunner.js',
        'app/PodcastViewRunner.js',
        'app/Router.js',
        'app/Podcaster.js'
    ];

gulp.task('clean', () => {
    return del(['assets/js/*.js']);
});

gulp.task('concat', () => {
    return gulp.src(files)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('assets/js'))
});

gulp.task('minify', () => {
    return gulp.src(files)
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'))
});

gulp.task('serve:min', ['clean', 'minify'], () => {
    exec('node ./node_modules/http-server/bin/http-server ./ -p 3000 -g --cors', (err) => {
    });
});

gulp.task('serve', ['clean', 'concat'], () => {
    exec('node ./node_modules/http-server/bin/http-server ./ -p 3000 -g --cors', (err,stdout, stderr) => {
    });
});